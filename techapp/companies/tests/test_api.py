import json
import pytest
from unittest import TestCase

from django.test import Client
from django.urls import reverse

from companies.models import Company, exception_function, ModelException


@pytest.mark.django_db
class BasicCompanyTest(TestCase):

    def setUp(self) -> None:
        self.client = Client()
        self.companies_url = reverse("companies-list")

    def tearDown(self) -> None:
        return super().tearDown()


class TestGetCompanies(BasicCompanyTest):

    def test_zero_companies_should_return_empty_list(self):
        response = self.client.get(self.companies_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), [])

    def test_one_company_exists_should_succeed(self):
        company = Company.objects.create(name="Amazon")

        response = self.client.get(self.companies_url)
        response_content = json.loads(response.content).pop()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_content.get("name"), company.name)
        self.assertEqual(response_content.get("status"), "Hiring")
        self.assertEqual(response_content.get("application_link"), "")
        self.assertEqual(response_content.get("notes"), "")

        company.delete()


class TestPostCompanies(BasicCompanyTest):

    def test_create_company_without_argument_should_fail(self):
        response = self.client.post(path=self.companies_url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            json.loads(response.content), {"name": ["This field is required."]}
        )

    def test_create_existing_company_should_fail(self):
        Company.objects.create(name="Apple")

        response = self.client.post(path=self.companies_url,
                                    data={"name": "Apple"})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            json.loads(response.content),
            {"name": ["company with this name already exists."]}
        )

    def test_create_company_with_only_name_all_fields_should_be_default(self):
        response = self.client.post(path=self.companies_url,
                                    data={"name": "Apple"})
        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response_content.get("name"), "Apple")
        self.assertEqual(response_content.get("status"), "Hiring")
        self.assertEqual(response_content.get("application_link"), "")
        self.assertEqual(response_content.get("notes"), "")

    def test_create_company_with_layoffs_status_should_succeed(self):
        response = self.client.post(path=self.companies_url,
                                    data={"name": "Apple", "status": "Layoffs"})
        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response_content.get("status"), "Layoffs")

    def test_create_company_with_wrong_status_should_fail(self):
        response = self.client.post(path=self.companies_url,
                                    data={"name": "Apple", "status": "Wrong"})
        self.assertEqual(response.status_code, 400)
        self.assertIn("Wrong", str(response.content))
        self.assertIn("is not a valid choice.", str(response.content))

    @pytest.mark.xfail
    def test_should_be_ok_if_fails(self):
        self.assertEqual(1, 2)

    @pytest.mark.skip
    def test_should_be_skipped(self):
        self.assertEqual(1, 2)

    def test_model_exception_should_pass(self):
        with pytest.raises(ModelException) as err:
            exception_function()
        self.assertEqual(str(err.value), "Model Exception")
