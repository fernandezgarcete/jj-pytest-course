import logging


LOGGER = logging.getLogger("TECHAPP_LOGS")


def logger_function():
    try:
        raise ValueError("ValueError Exception")
    except ValueError as e:
        LOGGER.warning(f'I am logging {str(e)}')
